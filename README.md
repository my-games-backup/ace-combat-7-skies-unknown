# Ace Combat 7 - Skies Unknown

**Game Details**


![wallpaper](image.jpg)

File Location
`C:\Users\"USER_NAME"\AppData\Local\BANDAI NAMCO Entertainment`
id `76561197960267366` is the latest updated one!

XBOX 360 Controller Emulator  <br>
```
Software Version : 3.2.10.82(2018-07-07) 64-bit
Game Settings    : XInput Files > 64-bit v1.1
```

**Last Saved Mission** <br> 
Mission 16 - Last Hope

Mission List: <https://acecombat.fandom.com/wiki/Ace_Combat_7:_Skies_Unknown/Missions>